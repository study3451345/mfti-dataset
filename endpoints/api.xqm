module namespace lipers-rdf = "lipers24.ru/rdf/v0.1/user";

import module namespace funct="simplex-frame/v0.1/funct" at "../../simplex-frame/functions.xqm";

declare 
  %rest:GET
  %rest:path("/api/v01/dataset/create")
  %output:method("text")
function lipers-rdf:getList(){
    funct:tpl('api/getList', map{})
};
declare 
  %rest:GET
  %rest:path("/api/v01/dataset/dbinfo")
  %output:method("json")
function lipers-rdf:getInfo(){
    funct:tpl('api/getInfo', map{})
};

declare 
  %rest:GET
  %rest:path("/api/v01/dataset/download")
function lipers-rdf:getCargos(){  
  let $res := funct:tpl('api/getCargos', map{})
  let $Content-Disposition := "attachment; filename=" || 'cargos-' || random:uuid() || '.zip'
  return
    (
       <rest:response>
        <http:response status="200">
          <http:header name="Content-Disposition" value="{$Content-Disposition}" />
          <http:header name="Content-type" value="application/octet-stream"/>
        </http:response>
      </rest:response>,  
      xs:base64Binary($res)
    )
};