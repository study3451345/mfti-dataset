module namespace dataSet = "garpix.com/dataset/gls/api/v0.1";

import module namespace config="simplex-frame/v0.1/config" at "../../simplex-frame/config.xqm";

(:~
 : Permissions:
 : Проверяет токен в заголовке
 :)
declare
  %perm:check('/api/v01')
function dataSet:check-admin() {
  let $token := config:param('accessToken')
  let $at := substring-after(request:header('Authorization'), 'Bearer ')
  where not($token = $at) and config:param('authMode')='true'
  return
    <err:auth01>Ошибка авторизации. Проверьте токен в заголовке: Authorization: Bearer {{access_token}}</err:auth01>
};