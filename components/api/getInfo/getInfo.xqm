module namespace getInfo="getInfo";

declare function getInfo:main($params as map(*)){
  let $path := $params?_config('dataPath')
  let $files := getInfo:filesList($path)
  return
    map{
      "data": getInfo:json(getInfo:info($files))
    }
};

declare function getInfo:json($info)as element(json) {
  <json type="object">
    {
      for $i in $info
      return
        <cargo__space type="object">
          <type type="string">{$i?1}</type>
          <boxes__count type="number">{$i?2}</boxes__count>
          <avg__density type="number">{$i?3}</avg__density>
        </cargo__space>
    }
  </json>
};

declare function getInfo:info($files as xs:string*){
  let $res :=
  for $i in $files[matches(., '.json$')]
  let $p := tokenize($i, '--')
  let $type := $p[1]
  let $count := xs:integer($p[2])
  let $density := xs:integer($p[3])
  group by $type
  return
    [$type, sum($count), round(avg($density))]
return
  $res
};

declare function getInfo:filesList($path as xs:string) as xs:string* {
  file:list($path)
};