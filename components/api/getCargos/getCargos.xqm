module namespace getCargos="getCargos";

declare function getCargos:main($params as map(*)){
  let $path := $params?_config('dataPath')
  let $cargos :=
    request:parameter('cargos') ?? tokenize(request:parameter('cargos'), ',') !! ()
  let $files := getCargos:filesList($path)
  let $data :=
    for $i in $files
    let $id := replace($i, '.*_algoritm_(\d{0,6}).json$', '$1')
    where $id = $cargos[position()]
    order by $id
    let $f := file:read-binary($path||$i)
    return
      [$id || '.json', $f]
  
  return
    map{
      "data": archive:create($data?1, $data?2)
    }
};

declare function getCargos:filesList($path as xs:string) as xs:string* {
  file:list($path)
};