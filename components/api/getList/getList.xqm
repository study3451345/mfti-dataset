module namespace getList="getList";

declare function getList:main($params as map(*)){
  let $path := $params?_config('dataPath')
  let $limitTrainig := 
    request:parameter('limit-training') ?? request:parameter('limit-training') !! 100
  let $limitTest := 
    request:parameter('limit-test') ?? request:parameter('limit-test') !! round($limitTrainig div 10)
  let $type := request:parameter('type') ?? request:parameter('type') !! 'pallet'
  let $data := getList:getList(getList:filesList($path), $type, [$limitTrainig, $limitTest])
  let $res :=
    if($data[1]?1 > 0 and $data[2]?1 > 0)
    then(
      getList:serialize($data, $type)
    )
    else(
      <json type="object"><error type="string">Указан_слишком_большой_лимит</error></json>
    )
  return
    map{
      "data":
        replace(
          serialize($res, map{"method":"json", "media-type":"application/json"}),
          '\s', ''
        )
    }
};


declare function getList:info($params as map(*)){
    let $path := $params?_config('dataPath')
    let $files := file:list($path)
    
    let $res :=
      for $i in $files
      let $p := tokenize($i, '--')
      let $type := $p[1]
      let $count := xs:integer($p[2])
      let $density := xs:integer($p[3])
      group by $type
      return
        [$type, sum($count), round(avg($density))]
    return
      $res
};


declare function getList:d(
  $data as xs:string*,
  $limit,
  $sum,
  $pack
){
  if($sum?1<$limit?1)
  then(
    getList:d(
      $data[position()>1],
      $limit,
      [$sum?1 + xs:integer(tokenize($data[1], '--')[2]), $sum?2],
      [insert-before($pack?1, 1, replace($data[1], '.*_algoritm_(\d{0,6}).json$', '$1')), ()]
    )
  )
  else(
    if($sum?2<$limit?2)
    then(
      getList:d(
        $data[position()>1],
        $limit,
        [$sum?1, $sum?2 + xs:integer(tokenize($data[1], '--')[2])],
        [$pack?1, insert-before($pack?2, 1, replace($data[1], '.*_algoritm_(\d{0,6}).json$', '$1'))]
      )
    )
    else(
      ([$sum?1, $pack?1], [$sum?2, $pack?2])
    )
  )
};

declare function getList:getList($files as xs:string*, $type as xs:string, $limit)
{
  let $data :=
    for $i in $files
    let $p := tokenize($i, '--')
    let $cargospaceType := $p[1]
    let $count := xs:integer($p[2])
    let $density := xs:integer($p[3])
    where $cargospaceType = $type
    order by random:double()
    return
      $i
  return
    getList:d($data, $limit, [0,0], [(),()])
};

declare function getList:filesList($path as xs:string) as xs:string* {
  file:list($path)
};

declare function getList:serialize($data, $type) as element(json) {
  <json type="object">
    <cargo__space type="object">
      <type type="string">{$type}</type>
    </cargo__space>
    <training__sample type="object">
      <boxes__count type="number">{$data[1]?1}</boxes__count>
      <cargos type="array">{
        for-each($data[1]?2,function($v){<_ type="number">{$v}</_>})
      }</cargos>
    </training__sample>
    <test__sample type="object">
      <boxes__count type="number">{$data[2]?1}</boxes__count>
      <cargos type="array">{
        for-each($data[2]?2,function($v){<_ type="number">{$v}</_>})
      }</cargos>
    </test__sample>
  </json>
};