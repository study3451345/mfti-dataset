# API для доступа к датасету

Предоставляет методы для получения набора данных из GLS:
- `http://dataset.ultra-production.ru:8086/api/v01/getlist?limit-training=1000`
- `http://dataset.ultra-production.ru:8086/api/v01/getCargos?cargos=33633,34039,34331,34657,31992,33956,32171,34223,33325,32461,34382`
